<?php

class NoticiasService {

    public function ordenar($dados) {
        $usuario = new UsuariosModel($dados->usuarioId);
        $perfil = $usuario->getPerfil();

        if($perfil != null) {
            $listaNoticias = $this->carregarNoticias($dados->noticias, $perfil);

            usort($listaNoticias,
                function($noticia1, $noticia2) { return strcmp($noticia1->getDistancia(), $noticia2->getDistancia()); });

            foreach($listaNoticias as $noticia) {
                $ordem[] = $noticia->id;
            }
        }

        return $ordem;
    }

    private function carregarNoticias($noticias, $perfil) {
        foreach ($noticias as $noticia) {
            $keywords = [];

            foreach($noticia->keywords as $keyword) {
                $keywords[trim($keyword)] = 1;
            }

            $item = new NoticiasModel($noticia->id, $keywords);
            $item->setDistancia($this->calcularDistancia($perfil, $item));

            $listaNoticias[] = $item;
        }

        return $listaNoticias;
    }

    private function calcularDistancia($perfil, $noticia) {
        $cosineDistance = new CosineDistanceService();

        return $cosineDistance->calcularDistancia($perfil, $noticia->keywords);
    }
}