<?php

class SistemaService {

    public function validarToken($token) {
        $sistema = new SistemaModel();

        return $sistema->validarToken($token);
    }

    public function gravarRequisicao($requisicao) {
        $sistema = new SistemaModel();
        $sistema->gravarRequisicao($requisicao);
    }
}