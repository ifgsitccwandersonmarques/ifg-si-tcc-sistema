<?php

class PortaisService {

    public function index() {
        $portal = new PortaisModel($_SESSION['id']);

        $dados['qtdUsuarios'] = $portal->contarUsuarios();
        $dados['qtdKeywords'] = $portal->contarKeywords();
        $dados['qtdRequisicoes'] = $portal->contarRequisicoes();
        $dados['qtdTempo'] = $portal->calcularTempoResposta();
        $dados['tabelaRequisicoes'] = $portal->buscarRequisicoes(0, 3);

        return $dados;
    }

    public function login($email, $senha) {
        $portal = new PortaisModel();
        $acesso = $portal->login($email, $senha);
        if($acesso) {
            $_SESSION['id'] = $portal->getId();
            $_SESSION['admin'] = $portal->getAdmin();
        }

        return $acesso;
    }

    public function logout() {
        session_destroy();
    }

    public function editarConta($dados) {
        if(isset($dados['id']) && $_SESSION['admin']) {
            $portal = new PortaisModel($dados['id']);
            unset($dados['id']);
        }
        else $portal = new PortaisModel($_SESSION['id']);

        if(!is_null($dados)) {
            $portal->setEmail($dados['email']);
            if(!empty($dados['senha'])) $portal->setSenha(md5($dados['senha']));
            $portal->setNome($dados['nome']);
            $portal->setUrl($dados['url']);

            $portal->editarConta();
        }

        $campos["token"] = $portal->getToken();
        $campos["email"] = $portal->getEmail();
        $campos["senha"] = $portal->getSenha();
        $campos["nome"] = $portal->getNome();
        $campos["url"] = $portal->getUrl();
        $campos["id"] = $portal->getId();

        return $campos;
    }

    public function novaConta($dados) {
        $portal = new PortaisModel();
        $portal->setToken(md5(md5(substr($dados['senha'], 0, 10).time().$dados['url'])));
        $portal->setEmail($dados['email']);
        $portal->setSenha(md5($dados['senha']));
        $portal->setNome($dados['nome']);
        $portal->setUrl($dados['url']);
        $portal->setAdmin(0);

        $portal->novaConta();
    }

    public function apagarConta($id) {
        if(isset($id) && $_SESSION['admin'] == 1) {
            $portal = new PortaisModel($id);
            $portal->apagarConta();
        }
    }

    public function verRelatorios($inicio) {
        $portal = new PortaisModel($_SESSION['id']);

        return $portal->buscarRequisicoes($inicio, 10);
    }

    public function verContas($inicio) {
        $portal = new PortaisModel($_SESSION['id']);
        if($portal->getAdmin() == 1) return $portal->buscarContas($inicio, 10);
        else return -1;
    }

    public function topKeywords($n) {
        $portal = new PortaisModel($_SESSION['id']);
        $keywords = $portal->topKeywords($n);

        $cols[] = ["label" => "Keyword", "type" => "string"];
        $cols[] = ["label" => "Quantidade", "type" => "number"];

        $rows = [];
        if(!empty($keywords)) {
            foreach ($keywords as $keyword) {
                $celulas = [];

                foreach ($keyword as $valor) {
                    $celulas[]["v"] = $valor;
                }

                $rows[]["c"] = $celulas;
            }
        }

        $tabela["cols"] = $cols;
        $tabela["rows"] = $rows;

        return $tabela;
    }

    public function sucesso() {
        $portal = new PortaisModel($_SESSION['id']);
        $resultados = $portal->sucesso();

        $cols[] = ["label" => "Resultado", "type" => "string"];
        $cols[] = ["label" => "Porcentagem", "type" => "number"];

        $rows = [];
        if(!empty($resultados)) {
            foreach ($resultados as $resultado => $value) {
                $celulas = [];
                $celulas[]["v"] = $resultado;
                $celulas[]["v"] = $value;

                $rows[]["c"] = $celulas;
            }
        }

        $tabela["cols"] = $cols;
        $tabela["rows"] = $rows;

        return $tabela;
    }

    public function contagemMeses() {
        $portal = new PortaisModel($_SESSION['id']);
        $meses = $portal->contagemMeses();

        $cols[] = ["label" => "Mês", "type" => "string"];
        $cols[] = ["label" => "Quantidade", "type" => "number"];

        foreach ($meses as $mes) {
            $celulas = [];

            foreach ($mes as $valor) {
                $celulas[]["v"] = $valor;
            }

            $rows[]["c"] = $celulas;
        }

        $tabela["cols"] = $cols;
        $tabela["rows"] = $rows;

        return $tabela;
    }

}