<?php

class UsuariosService {

    public function cadastrar($dados) {
        $usuario = new UsuariosModel();
        $usuario->setIp($_SERVER['REMOTE_ADDR']);
        $usuario->cadastrar($dados->token);

        if($usuario->getId() > 0) {
            $resposta['status'] = "OK";
            $resposta['dados'] = $usuario->getId();
        }
        else {
            $resposta['status'] = "FAIL";
        }

        return $resposta;
    }

    public function salvarKeywords($dados) {
        $usuario = new UsuariosModel($dados->usuarioId);

        if($usuario->salvarKeywords($dados->keywords) == null) {
            $resposta['status'] = "OK";
        }
        else {
            $resposta['status'] = "FAIL";
        }

        return $resposta;
    }
}