<?php

class CosineDistanceService {

    public function calcularDistancia($vetor1, $vetor2) {
        $similaridade = $this->produtoEscalar($vetor1, $vetor2) / ($this->normaL2($vetor1) * $this->normaL2($vetor2));
        return 1 - $similaridade;
    }

    private function produtoEscalar($vetor1, $vetor2) {
        $total = 0;

        foreach (array_keys($vetor1) as $chave1) {
            foreach (array_keys($vetor2) as $chave2) {
                if ($chave1 === $chave2) $total += $vetor1[$chave1] * $vetor2[$chave2];
            }
        }

        return $total;
    }

    private function normaL2($vetor) {
        $total = 0;

        foreach (array_values($vetor) as $valor) {
            $total += $valor * $valor;
        }

        $total = sqrt($total);
        return $total;
    }
}