-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           5.7.11 - MySQL Community Server (GPL)
-- OS do Servidor:               Win32
-- HeidiSQL Versão:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura para tabela ifg-si-tcc.keywords
CREATE TABLE IF NOT EXISTS `keywords` (
  `usuario_id` int(11) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `dataAcesso` datetime NOT NULL,
  KEY `keywords` (`keyword`),
  KEY `usuario_id` (`usuario_id`),
  CONSTRAINT `keywords_usuarios` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela ifg-si-tcc.keywords: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `keywords` DISABLE KEYS */;
/*!40000 ALTER TABLE `keywords` ENABLE KEYS */;


-- Copiando estrutura para tabela ifg-si-tcc.portais
CREATE TABLE IF NOT EXISTS `portais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(500) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `senha` varchar(100) NOT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `dataAlteracao` datetime NOT NULL,
  `admin` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela ifg-si-tcc.portais: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `portais` DISABLE KEYS */;
INSERT INTO `portais` (`id`, `token`, `email`, `senha`, `nome`, `url`, `dataAlteracao`, `admin`) VALUES
	(1, '', 'wanderson.marques@ifg.edu.br', 'd55ae306d87d2c395f6ed6fbdeef7e3d', '', '', '2017-03-17 23:33:23', 1),
	(2, '8b70c0aa05c41bb1e277ebbaa44b769c', 'wdsmarques@gmail.com', 'd55ae306d87d2c395f6ed6fbdeef7e3d', '.N', 'http://localhost/portal', '2017-03-17 23:33:13', 0);
/*!40000 ALTER TABLE `portais` ENABLE KEYS */;


-- Copiando estrutura para tabela ifg-si-tcc.requisicoes
CREATE TABLE IF NOT EXISTS `requisicoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `portal_id` int(11) NOT NULL,
  `modulo` varchar(50) NOT NULL,
  `tarefa` varchar(50) NOT NULL,
  `entrada` longtext NOT NULL,
  `saida` longtext NOT NULL,
  `dataProcessamento` timestamp NOT NULL,
  `tempoProcessamento` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `requisicoes_portais` (`portal_id`),
  CONSTRAINT `requisicoes_portal` FOREIGN KEY (`portal_id`) REFERENCES `portais` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela ifg-si-tcc.requisicoes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `requisicoes` DISABLE KEYS */;
/*!40000 ALTER TABLE `requisicoes` ENABLE KEYS */;


-- Copiando estrutura para tabela ifg-si-tcc.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `portal_id` int(11) NOT NULL,
  `ip` varchar(100) NOT NULL,
  `dataCadastro` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usuarios_portais` (`portal_id`),
  CONSTRAINT `usuarios_portal` FOREIGN KEY (`portal_id`) REFERENCES `portais` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela ifg-si-tcc.usuarios: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
