<?php

class SistemaController extends Controller {

    public function executarTarefa($modulo, $tarefa, $dados) {
        $nome_controller = ucfirst($modulo) . 'Controller';

        $controller = new $nome_controller();
        $controller->$tarefa($dados);
    }

    public function solicitarLogin($dados) {
        $this->renderizar($dados, "login");
    }

    public function exibirErro($mensagem) {
        $resposta['status'] = 'FAIL';
        $resposta['mensagem'] = $mensagem;

        $this->renderizar($resposta, "jsonView");
    }

    public function validarToken($token) {
        $service = new SistemaService();

        return $service->validarToken($token);
    }

    public function gravarRequisicao($token, $modulo, $tarefa, $entrada, $saida, $tempo) {
        $service = new SistemaService();
        $requisicao = new RequisicoesModel($token, $modulo, $tarefa, $entrada, $saida, $tempo);

        $service->gravarRequisicao($requisicao);
    }
}