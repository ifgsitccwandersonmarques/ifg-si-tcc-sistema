<?php

class UsuariosController extends Controller {

    public function cadastrar($dados) {
        $service = new UsuariosService();
        $resposta = $service->cadastrar($dados);

        $this->renderizar($resposta, "jsonView");
    }

    public function salvarKeywords($dados) {
        $service = new UsuariosService();
        $resposta = $service->salvarKeywords($dados);

        $this->renderizar($resposta, "jsonView");
    }
}