<?php

class NoticiasController extends Controller {

    public function ordenar($dados) {
        $service = new NoticiasService();
        $ordem = $service->ordenar($dados);

        if(count($ordem) > 0) {
            $resposta['status'] = "OK";
            $resposta['dados'] = $ordem;
        }
        else $resposta['status'] = "FAIL";

        $this->renderizar($resposta, "jsonView");
    }
}