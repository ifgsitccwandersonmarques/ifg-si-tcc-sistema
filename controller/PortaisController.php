<?php

class PortaisController extends Controller {

    public function index($dados) {
        $service = new PortaisService();
        if($dados == null) $dados = [];
        $dados = array_merge($dados, $service->index());

        $this->renderizar($dados, "dashboard");
    }

    public function login($dados) {
        $service = new PortaisService();

        if($service->login($dados['email'], $dados['senha'])) $this->index(["mensagem"=>"Login realizado com sucesso. Seja bem-vindo novamente.", "tipo"=> "success"]);

        else $this->renderizar(["mensagem"=>"E-mail e/ou senha inválidos.", "tipo"=> "danger"], "login");
    }

    public function logout($dados) {
        $service = new PortaisService();
        $service->logout();

        $this->renderizar(["mensagem"=>"Você foi desconectado com sucesso.", "tipo"=> "success"], "login");
    }

    public function editarConta($dados) {
        $service = new PortaisService();

        $msg = [];
        if(!is_null($dados)) $msg = ["mensagem"=>"Dados atualizados com sucesso.", "tipo"=> "success"];

        if(isset($_GET['id'])) $dados['id'] = $_GET['id'];
        $campos = $service->editarConta($dados);

        $this->renderizar(array_merge($campos, $msg), "conta");
    }

    public function novaConta($dados) {
        $service = new PortaisService();

        if($dados != null) {
            $service->novaConta($dados);

            $contas['tabela'] = $service->verContas(0);

            if($contas['tabela'] == -1) $this->renderizar(["mensagem"=>"Acesso Negado", "tipo"=> "danger"], "dashboard");
            else $this->renderizar(array_merge($contas, ["inicio" => 0, "mensagem"=>"Conta criada com sucesso.", "tipo"=> "success"]), "contas");
        }

        else $this->renderizar($dados, "novaconta");
    }

    public function apagarConta($dados) {
        $service = new PortaisService();
        $service->apagarConta($_GET['id']);
        $contas['tabela'] = $service->verContas(0);

        if($contas['tabela'] == -1) $this->renderizar(["mensagem"=>"Acesso Negado", "tipo"=> "danger"], "dashboard");
        else $this->renderizar(array_merge($contas, ["mensagem"=>"Conta apagada com sucesso.", "tipo"=> "success"]), "contas");
    }

    public function verContas($dados) {
        $service = new PortaisService();
        $contas['tabela'] = $service->verContas($_GET['inicio']);

        if($contas['tabela'] == -1) $this->renderizar(["mensagem"=>"Acesso Negado", "tipo"=> "danger"], "dashboard");
        else $this->renderizar(array_merge($contas, ["inicio" => $_GET['inicio']]), "contas");
    }

    public function verRelatorios($dados) {
        $service = new PortaisService();
        $requisicoes['tabela'] = $service->verRelatorios($_GET['inicio']);

        $this->renderizar(array_merge($requisicoes, ["inicio" => $_GET['inicio']]), "relatorios");
    }

    public function topKeywords($dados) {
        $service = new PortaisService();
        $tabela = $service->topKeywords(5);

        $this->renderizar($tabela, "jsonView");
    }

    public function sucesso() {
        $service = new PortaisService();
        $tabela = $service->sucesso();

        $this->renderizar($tabela, "jsonView");
    }

    public function contagemMeses($dados) {
        $service = new PortaisService();
        $tabela = $service->contagemMeses();

        $this->renderizar($tabela, "jsonView");
    }
}