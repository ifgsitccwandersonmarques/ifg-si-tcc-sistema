<?php

require_once '../configuracao.php';

session_start();

(empty($_GET['modulo']))? $modulo = "portais" : $modulo = $_GET['modulo'];
(empty($_GET['tarefa']))? $tarefa = "index" : $tarefa = $_GET['tarefa'];
(empty($_POST))? $dados = json_decode(file_get_contents('php://input')) : $dados = $_POST;

$sistema = new SistemaController();
if (isset($_SESSION['id']) || $tarefa == "login") {
    $sistema->executarTarefa($modulo, $tarefa, $dados);
}
else {
    $sistema->solicitarLogin($dados);
}