<?php

class Conexao {
    private static $instance = null;
    private $pdo;

    private function __construct() {
        $this->pdo = new PDO("mysql:host=localhost;dbname=ifg-si-tcc;charset=utf8", "root", "");
        $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, FALSE);
    }

    public static function getInstance() {
        if (self::$instance == null) {
            self::$instance = new Conexao();
        }
        return self::$instance;
    }

    public function getPDO() {
        return $this->pdo;
    }

    public function executarQuery($query, $params) {
        $stmt = $this->pdo->prepare($query);

        for($i=0; $i<count($params); $i++) {
            $stmt->bindValue($i+1, $params[$i]);
        }
        $stmt->execute();

        return $stmt;
    }

}