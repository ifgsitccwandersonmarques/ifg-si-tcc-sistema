<?php

function carregarConexao() {
    require_once 'Conexao.php';
}

function carregarControllers($classe) {
    $arquivo = "../controller/{$classe}.php";
    if(file_exists($arquivo)) require_once $arquivo;
}

function carregarServices($classe) {
    $arquivo = "../service/{$classe}.php";
    if(file_exists($arquivo)) require_once $arquivo;
}

function carregarModels($classe) {
    $arquivo = "../model/{$classe}.php";
    if(file_exists($arquivo)) require_once $arquivo;
}

spl_autoload_register("carregarConexao");
spl_autoload_register("carregarControllers");
spl_autoload_register("carregarServices");
spl_autoload_register("carregarModels");