<?php

require_once '../configuracao.php';

$tempo_inicio = microtime(true);

$modulo = $_GET['modulo'];
$tarefa = $_GET['tarefa'];
$dados = json_decode(file_get_contents('php://input'));

$sistema = new SistemaController();
if ($sistema->validarToken($dados->token)) {
    $sistema->executarTarefa($modulo, $tarefa, $dados);
}
else {
    $sistema->exibirErro("Token inválido");
}

$tempo_execucao = (microtime(true) - $tempo_inicio);

$sistema->gravarRequisicao($dados->token, $modulo, $tarefa, file_get_contents('php://input'), json_encode($GLOBALS['saida']), $tempo_execucao);