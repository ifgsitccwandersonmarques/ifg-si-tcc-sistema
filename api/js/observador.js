
function salvarKeywords() {
    var dados = JSON.stringify({
        token: 'INSERIR_TOKEN_AQUI',
        usuarioId: $.cookie('id'),
        keywords: $('meta[name=keywords]').attr("content").split(',')
    });

    $.ajax({
        type: 'POST',
        url: 'http://localhost/ifg-si-tcc/api/index.php?modulo=usuarios&tarefa=salvarKeywords',
        data: dados,
        dataType: 'JSON'
    });
}

if($.cookie('id') == null) {
    var dados = JSON.stringify({
        token: 'INSERIR_TOKEN_AQUI'
    });

    $.ajax({
        type: 'POST',
        url: 'http://localhost/ifg-si-tcc/api/index.php?modulo=usuarios&tarefa=cadastrar',
        data: dados,
        dataType: 'JSON',
        success: function (resposta) {
            $.cookie('id', JSON.parse(resposta.dados), { expires: 365 });
            salvarKeywords();
        },
    });
}
else {
    salvarKeywords();
}

