<?php

class RequisicoesModel {
    private $token;
    private $modulo;
    private $tarefa;
    private $entrada;
    private $saida;
    private $tempo;

    function __construct($token, $modulo, $tarefa, $entrada, $saida, $tempo) {
        $this->token = $token;
        $this->modulo = $modulo;
        $this->tarefa = $tarefa;
        $this->entrada = $entrada;
        $this->saida = $saida;
        $this->tempo = $tempo;
    }

    public function getToken() {
        return $this->token;
    }

    public function getModulo() {
        return $this->modulo;
    }

    public function getTarefa() {
        return $this->tarefa;
    }

    public function getEntrada() {
        return $this->entrada;
    }

    public function getSaida() {
        return $this->saida;
    }

    public function getTempo() {
        return $this->tempo;
    }

    public function setToken($token) {
        $this->token = $token;
    }

    public function setModulo($modulo) {
        $this->modulo = $modulo;
    }

    public function setTarefa($tarefa) {
        $this->tarefa = $tarefa;
    }

    public function setEntrada($entrada) {
        $this->entrada = $entrada;
    }

    public function setSaida($saida) {
        $this->saida = $saida;
    }

    public function setTempo($tempo) {
        $this->tempo = $tempo;
    }
}