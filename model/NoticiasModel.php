<?php

class NoticiasModel {
    public $id;
    public $keywords;
    public $distancia;

    public function __construct($id, $keywords) {
        $this->id = $id;
        $this->keywords = $keywords;
    }

    public function getId() {
        return $this->id;
    }

    public function getKeywords() {
        return $this->keywords;
    }

    public function getDistancia() {
        return $this->distancia;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setKeywords($keywords) {
        $this->keywords = $keywords;
    }

    public function setDistancia($distancia) {
        $this->distancia = $distancia;
    }
}