<?php

class SistemaModel {

    public function validarToken($token) {
        $sql = "SELECT * FROM portais WHERE token = ?;";
        $stmt = Conexao::getInstance()->executarQuery($sql, [$token]);

        if($stmt->rowCount() == 1) return true;
        else return false;
    }

    public function gravarRequisicao($requisicao) {
        $sql = "INSERT INTO requisicoes(portal_id, modulo, tarefa, entrada, saida, dataProcessamento, tempoProcessamento) VALUES((SELECT id FROM portais WHERE token = ?), ?, ?, ?, ?, CURRENT_TIMESTAMP, ? );";
        Conexao::getInstance()->executarQuery($sql, [$requisicao->getToken(), $requisicao->getModulo(), $requisicao->getTarefa(), $requisicao->getEntrada(), $requisicao->getSaida(), $requisicao->getTempo()]);
    }
}