<?php

class UsuariosModel {
    private $id;
    private $perfil;
    private $ip;

    public function __construct($id = 0) {
        $this->id = $id;
    }

    public function cadastrar($token) {
        $sql = "INSERT INTO usuarios(portal_id, ip, dataCadastro) VALUES((SELECT id FROM portais WHERE token = ?), ?, CURRENT_TIMESTAMP);";
        Conexao::getInstance()->executarQuery($sql, [$token, $this->ip]);

        $this->id = intval(Conexao::getInstance()->getPDO()->lastInsertId());
    }

    public function salvarKeywords($keywords) {
        foreach ($keywords as $keyword) {
            $sql = "INSERT INTO keywords(usuario_id, keyword, dataAcesso) VALUES(?,?, CURRENT_TIMESTAMP);";
            $stmt = Conexao::getInstance()->executarQuery($sql, [$this->id, trim($keyword)]);
            if ($stmt->errorCode() !== "00000") $errors[] = $stmt->errorCode();
        }

        return $errors;
    }

    private function construirPerfil() {
        $sql = "SELECT keyword, SUM(365-(TIMESTAMPDIFF(DAY, dataAcesso, CURRENT_TIMESTAMP))) AS valor FROM keywords WHERE usuario_id = ? AND (TIMESTAMPDIFF(YEAR, dataAcesso, CURRENT_TIMESTAMP) < 1) GROUP BY keyword ORDER BY valor DESC;";
        $stmt = Conexao::getInstance()->executarQuery($sql, [$this->id]);

        while ($dados = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $perfil[$dados['keyword']] = floatval($dados['valor']);
        }

        $this->perfil = $perfil;
    }

    public function getId() {
        return $this->id;
    }

    public function getPerfil() {
        $this->construirPerfil();
        return $this->perfil;
    }

    public function getIp() {
        return $this->ip;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setPerfil($perfil) {
        $this->perfil = $perfil;
    }

    public function setIp($ip) {
        $this->ip = $ip;
    }
}