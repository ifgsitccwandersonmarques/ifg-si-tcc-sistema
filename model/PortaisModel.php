<?php

class PortaisModel {
    private $id;
    private $token;
    private $email;
    private $senha;
    private $nome;
    private $url;
    private $admin;

    public function __construct($id = 0) {
        $this->id = $id;
        if($id > 0) $this->carregarInformacoes();
    }

    public function login($email, $senha) {
        $sql = "SELECT * FROM portais WHERE email = ? AND senha = ?;";
        $stmt = Conexao::getInstance()->executarQuery($sql, [$email, md5($senha)]);

        if($stmt->rowCount() == 1) {
            $dados = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->id = $dados['id'];
            $this->admin = $dados['admin'];
            return true;

        }

        return false;
    }

    public function editarConta() {
        $sql = "UPDATE portais SET email = ?, nome = ?, url = ?, dataAlteracao = CURRENT_TIMESTAMP";
        $campos = [$this->email, $this->nome, $this->url];

        if(!empty($this->senha)) {
            $sql .= ", senha = ?";
            $campos[] = $this->senha;
        }

        $campos[] = $this->id;
        $sql .= " WHERE id = ?;";

        Conexao::getInstance()->executarQuery($sql, $campos);
        $this->carregarInformacoes();
    }

    public function novaConta() {
        $sql = "INSERT INTO portais (token, email, senha, nome, url, dataAlteracao, admin) VALUES (?, ?, ?, ?, ?, CURRENT_TIMESTAMP, ?);";
        Conexao::getInstance()->executarQuery($sql, [$this->token, $this->email, $this->senha, $this->nome, $this->url, $this->admin]);
    }

    public function apagarConta() {
        $sql = "DELETE FROM portais WHERE id = ?;";
        $stmt = Conexao::getInstance()->executarQuery($sql, [$this->id]);
    }

    public function buscarRequisicoes($limit, $qtd) {
        $sql = "SELECT * FROM requisicoes";
        if($this->admin == 0) {
            $sql .= " WHERE portal_id = ?";
            $campos[] = $this->id;
        }

        $sql .= " ORDER BY dataProcessamento DESC LIMIT ?, ?;";
        $campos[] = $limit;
        $campos[] = $qtd;

        $stmt = Conexao::getInstance()->executarQuery($sql, $campos);


        $dados = [];
        while ($dado = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $dados[] = $dado;
        }

        return $dados;
    }

    public function buscarContas($limit, $qtd) {
        $sql = "SELECT * FROM portais ORDER BY id ASC LIMIT ?, ?;";
        $stmt = Conexao::getInstance()->executarQuery($sql, [$limit, $qtd]);

        $dados = [];
        while ($dado = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $dados[] = $dado;
        }

        return $dados;
    }

    private function carregarInformacoes() {
        $sql = "SELECT * FROM portais WHERE id = ?;";
        $stmt = Conexao::getInstance()->executarQuery($sql, [$this->id]);

        $dados = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->id = $dados['id'];
        $this->token = $dados['token'];
        $this->email = $dados['email'];
        $this->senha = $dados['senha'];
        $this->nome = $dados['nome'];
        $this->url = $dados['url'];
        $this->admin = $dados['admin'];
    }

    public function contarUsuarios() {
        $sql = "SELECT COUNT(*) AS qtd FROM usuarios";
        if($this->admin == 0) {
            $sql .= " WHERE portal_id = ?;";
            $campos[]= $this->id;
        }
        $stmt = Conexao::getInstance()->executarQuery($sql, $campos);

        $dados = $stmt->fetch(PDO::FETCH_ASSOC);
        return $dados['qtd'];
    }

    public function contarKeywords() {
        $sql = "SELECT count(distinct keyword) AS qtd FROM keywords k INNER JOIN usuarios u on k.usuario_id = u.id";
        if($this->admin == 0) {
            $sql .= " WHERE portal_id = ?;";
            $campos[]= $this->id;
        }
        $stmt = Conexao::getInstance()->executarQuery($sql, $campos);

        $dados = $stmt->fetch(PDO::FETCH_ASSOC);
        return $dados['qtd'];
    }

    public function contarRequisicoes() {
        $sql = "SELECT count(*) as qtd FROM requisicoes";
        if($this->admin == 0) {
            $sql .= " WHERE portal_id = ?;";
            $campos[]= $this->id;
         }
        $stmt = Conexao::getInstance()->executarQuery($sql, $campos);

        $dados = $stmt->fetch(PDO::FETCH_ASSOC);
        return $dados['qtd'];
    }

    public function calcularTempoResposta() {
        $sql = "SELECT avg(tempoProcessamento) as qtd FROM requisicoes";
        if($this->admin == 0) {
            $sql .= " WHERE portal_id = ?;";
            $campos[]= $this->id;
        }
        $stmt = Conexao::getInstance()->executarQuery($sql, $campos);

        $dados = $stmt->fetch(PDO::FETCH_ASSOC);
        return $dados['qtd'];
    }

    public function sucesso() {
        $sql = "SELECT COUNT(*) as total FROM requisicoes WHERE modulo = \"noticias\" AND tarefa = \"ordenar\" AND saida = '{\"status\":\"FAIL\"}'";
        if($this->admin == 0) {
            $sql .= " AND portal_id = ?;";
            $campos[]= $this->id;
        }
        $stmt = Conexao::getInstance()->executarQuery($sql, $campos);
        $fail = $stmt->fetch(PDO::FETCH_ASSOC);

        $sql = "SELECT COUNT(*) as total FROM requisicoes WHERE modulo = \"noticias\" AND tarefa = \"ordenar\" AND saida != '{\"status\":\"FAIL\"}'";
        if($this->admin == 0) {
            $sql .= " AND portal_id = ?;";
            $campos[]= $this->id;
        }
        $stmt = Conexao::getInstance()->executarQuery($sql, $campos);
        $ok = $stmt->fetch(PDO::FETCH_ASSOC);

        $total = $fail['total']+$ok['total'];
        $dados = [];
        if($total >0 ) {
            $dados['Sucesso'] = $ok['total'];
            $dados['Falha'] = $fail['total'];
        }

        return $dados;
    }

    public function topKeywords($n) {
        $sql = "SELECT k.keyword, COUNT(keyword) AS qtd FROM keywords k INNER JOIN usuarios u ON k.usuario_id = u.id";
        if($this->admin == 0) {
            $sql .= " WHERE u.portal_id = ?";
            $campos[]= $this->id;
        }
        $campos[] = $n;
        $sql .= " GROUP BY keyword ORDER BY qtd DESC LIMIT ?;";
        $stmt = Conexao::getInstance()->executarQuery($sql, $campos);

        $dados = [];
        while ($dado = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $dados[] = $dado;
        }

        return $dados;
    }

    public function contagemMeses() {
        $sql = "SELECT MONTH(dataProcessamento) as mes, COUNT(*) as total FROM requisicoes WHERE";
        if($this->admin == 0) {
            $sql .= " portal_id = ? AND";
            $campos[]= $this->id;
        }
        $sql .= " YEAR(dataProcessamento) = YEAR(CURDATE()) GROUP BY MONTH(dataProcessamento);";
        $stmt = Conexao::getInstance()->executarQuery($sql, $campos);

        while ($dado = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $dados[$dado['mes']] = $dado;
        }

        for($i=1;$i<=12;$i++) {
            if(!isset($dados[$i])) {
                $dados[$i]['mes'] = $i;
                $dados[$i]['total'] = 0;
            }
        }
        usort($dados,
            function($a, $b) { return intval($a['mes']) - intval($b['mes']); });

        for($i=0;$i<12;$i++) {
            $dados[$i]['mes'] = $this->converterMes($dados[$i]['mes']);
        }

        return $dados;
    }

    private function converterMes($mes) {
        switch ($mes) {
            case 1:
                return "Janeiro";
            case 2:
                return "Fevereiro";
            case 3:
                return "Março";
            case 4:
                return "Abril";
            case 5:
                return "Maio";
            case 6:
                return "Junho";
            case 7:
                return "Julho";
            case 8:
                return "Agosto";
            case 9:
                return "Setembro";
            case 10:
                return "Outubro";
            case 11:
                return "Novembro";
            case 12:
                return "Dezembro";
        }
    }

    public function getId() {
        return $this->id;
    }

    public function getToken() {
        return $this->token;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getSenha() {
        return $this->senha;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getUrl() {
        return $this->url;
    }

    public function getAdmin() {
        return $this->admin;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setToken($token) {
        $this->token = $token;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setSenha($senha) {
        $this->senha = $senha;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function setUrl($url) {
        $this->url = $url;
    }

    public function setAdmin($admin) {
        $this->admin = $admin;
    }
}