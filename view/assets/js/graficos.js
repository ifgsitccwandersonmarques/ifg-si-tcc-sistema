google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawSucesso);
google.charts.setOnLoadCallback(drawTop);
google.charts.setOnLoadCallback(drawRequisicoes);

function drawSucesso() {
    var jsonData = $.ajax({
        type: 'POST',
        url: 'http://localhost/ifg-si-tcc/admin/index.php?modulo=portais&tarefa=sucesso',
        async: false,
    }).responseText;


    // Create our data table out of JSON data loaded from server.
    var data = new google.visualization.DataTable(jsonData);

    // Instantiate and draw our chart, passing in some options.
    var chart = new google.visualization.PieChart(document.getElementById('sucesso_div'));
    chart.draw(data);
}

function drawTop() {
    var jsonData = $.ajax({
        type: 'POST',
        url: 'http://localhost/ifg-si-tcc/admin/index.php?modulo=portais&tarefa=topKeywords',
        async: false,
    }).responseText;


    //Create our data table out of JSON data loaded from server.
    var data = new google.visualization.DataTable(jsonData);

    // Instantiate and draw our chart, passing in some options.
    var chart = new google.visualization.ColumnChart(document.getElementById('top_div'));
    chart.draw(data);
}

function drawRequisicoes() {
    var jsonData = $.ajax({
        type: 'POST',
        url: 'http://localhost/ifg-si-tcc/admin/index.php?modulo=portais&tarefa=contagemMeses',
        async: false,
    }).responseText;

    // Create our data table out of JSON data loaded from server.
    var data = new google.visualization.DataTable(jsonData);

    // Instantiate and draw our chart, passing in some options.
    var chart = new google.visualization.LineChart(document.getElementById('requisicoes_div'));
    chart.draw(data);
}