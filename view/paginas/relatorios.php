<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Notitia | Relatórios</title>
    <?php include '../view/includes/head.php'; ?>
    <link rel="stylesheet" type="text/css" href="../view/assets/css/dashboard.css">
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-2 col-md-2 top"><?php include '../view/includes/logo_top.php'; ?></div>
        <div class="hidden-xs col-sm-10 col-md-10 top"><?php include '../view/includes/data_top.php'; ?></div>
    </div>
    <div class="row">
        <div class="col-md-2"><?php include '../view/includes/menu.php'; ?></div>
        <div class="col-md-10 conteudo">

            <div class="row">
                <div class="col-md-12">
                    <div class="page-header"><h1>Relatórios <small>Informações detalhadas</small></h1></div>
                    <?php include '../view/includes/mensagem.php'; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form>
                        <div class="form-group">
                            <select class="form-control">
                                <option selected>Requisições Processadas</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-condensed">
                        <thead>
                        <tr>
                            <th class="col-xs-3 col-sm-1 col-md-1">Módulo</th>
                            <th class="col-xs-3 col-sm-1 col-md-1">Tarefa</th>
                            <th class="hidden-xs col-sm-4 col-md-4">Entrada</th>
                            <th class="hidden-xs col-sm-4 col-md-4">Saída</th>
                            <th class="col-xs-3 col-sm-1 col-md-1">Data</th>
                            <th class="col-xs-3 col-sm-1 col-md-1">Tempo (s)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(isset($dados['tabela'])) foreach ($dados['tabela'] as $linha) { ?>
                            <tr>
                                <td class="col-xs-3 col-sm-1 col-md-1"><?php echo $linha['modulo']; ?></td>
                                <td class="col-xs-3 col-sm-1 col-md-1"><?php echo $linha['tarefa']; ?></td>
                                <td class="hidden-xs col-sm-4 col-md-4">
                                    <div class="form-group">
                                        <textarea class="textolongo form-control"><?php echo $linha['entrada']; ?></textarea>
                                    </div>
                                </td>
                                <td class="hidden-xs col-sm-4 col-md-4">
                                    <div class="form-group">
                                        <textarea class="textolongo form-control"><?php echo $linha['saida']; ?></textarea>
                                    </div>
                                </td>
                                <td class="col-xs-3 col-sm-1 col-md-1"><?php echo $linha['dataProcessamento']; ?></td>
                                <td class="col-xs-3 col-sm-1 col-md-1"><?php echo round($linha['tempoProcessamento'], 5); ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                    <p class="paginacao">
                        <?php if($dados['inicio'] > 0) { ?>
                            <a href="index.php?modulo=portais&tarefa=verRelatorios&inicio=<?php echo $dados["inicio"]-10; ?>"><button class="btn btn-danger">Anterior</button></a>
                        <?php } ?>
                        <a href="index.php?modulo=portais&tarefa=verRelatorios&inicio=<?php echo $dados["inicio"]+10; ?>"><button class="btn btn-primary">Próxima</button></a>
                    </p>
                </div>
            </div>
        </div>

        <?php include '../view/includes/rodape.php'; ?>
    </div>
</body>
</html>