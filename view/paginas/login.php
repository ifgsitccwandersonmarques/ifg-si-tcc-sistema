<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Notitia | Login</title>
    <?php include '../view/includes/head.php'; ?>
    <link rel="stylesheet" type="text/css" href="../view/assets/css/login.css">
</head>
<body class="login">
<div class="container">

    <div class="centro">
        <div class="row">
            <div class="col-md-12">
                <p class="logotipo"><img src="../view/media/imagens/logo.png"></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form method="POST" action="index.php?modulo=portais&tarefa=login">
                    <p>Seja bem-vindo. <span>Por favor, realize seu login.</span></p>
                    <?php include '../view/includes/mensagem.php'; ?>

                    <div class="form-group">
                        <input type="text" placeholder="E-mail" name="email" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="password" placeholder="Senha" name="senha" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-default">Acessar</button>
                </form>
            </div>
        </div>
    </div>

</div>
</body>
</html>