<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Notitia | Dashboard</title>
    <?php include '../view/includes/head.php'; ?>
    <link rel="stylesheet" type="text/css" href="../view/assets/css/dashboard.css">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="../view/assets/js/graficos.js"></script>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-2 col-md-2 top"><?php include '../view/includes/logo_top.php'; ?></div>
        <div class="hidden-xs col-sm-10 col-md-10 top"><?php include '../view/includes/data_top.php'; ?></div>
    </div>
    <div class="row">
        <div class="col-md-2"><?php include '../view/includes/menu.php'; ?></div>

        <div class="col-md-10 conteudo">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header"><h1>Dashboard <small>Estatísticas gerais</small></h1></div>
                    <?php include '../view/includes/mensagem.php'; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a class="resumo azul" href="#">
                        <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                        <div class="numero"><?php echo $dados['qtdUsuarios']; ?></div>
                        <div class="legenda">Usuários</div>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a class="resumo vermelho" href="#">
                        <span class="glyphicon glyphicon-font" aria-hidden="true"></span>
                        <div class="numero"><?php echo $dados['qtdKeywords']; ?></div>
                        <div class="legenda">Palavras-chave</div>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a class="resumo laranja" href="#">
                        <span class="glyphicon glyphicon-tasks" aria-hidden="true"></span>
                        <div class="numero"><?php echo $dados['qtdRequisicoes']; ?></div>
                        <div class="legenda">Requisições Processadas</div>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <a class="resumo roxo" href="#">
                        <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                        <div class="numero"><?php echo round($dados['qtdTempo'], 2); ?>s</div>
                        <div class="legenda">Tempo Médio de Resposta</div>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <div class="panel panel-default graficopanel">
                        <div class="panel-heading">Sucesso em Recomendações</div>
                        <div class="panel-body"><div id="sucesso_div"></div></div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class="panel panel-default graficopanel">
                        <div class="panel-heading">Palavras-chave com Maior Incidência</div>
                        <div class="panel-body"><div id="top_div"></div></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <div class="panel panel-default graficopanel">
                        <div class="panel-heading">Requisições por Mês</div>
                        <div class="panel-body"><div id="requisicoes_div"></div></div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class="panel panel-default graficopanel">
                        <div class="panel-heading">Últimas Requisições</div>
                        <div class="panel-body">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th>Módulo</th>
                                    <th>Tarefa</th>
                                    <th>Data</th>
                                    <th>Tempo (s)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if(isset($dados['tabelaRequisicoes'])) foreach ($dados['tabelaRequisicoes'] as $linha) { ?>
                                    <tr>
                                        <td class="col-md-3"><?php echo $linha['modulo']; ?></td>
                                        <td class="col-md-3"><?php echo $linha['tarefa']; ?></td>
                                        <td class="col-md-3"><?php echo $linha['dataProcessamento']; ?></td>
                                        <td class="col-md-3"><?php echo round($linha['tempoProcessamento'], 5); ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div></div>
                </div>
            </div>
        </div>
    </div>

    <?php include '../view/includes/rodape.php'; ?>
</div>
</body>
</html>