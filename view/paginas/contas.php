<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Notitia | Relatórios</title>
    <?php include '../view/includes/head.php'; ?>
    <link rel="stylesheet" type="text/css" href="../view/assets/css/dashboard.css">
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-2 col-md-2 top"><?php include '../view/includes/logo_top.php'; ?></div>
        <div class="hidden-xs col-sm-10 col-md-10 top"><?php include '../view/includes/data_top.php'; ?></div>
    </div>
    <div class="row">
        <div class="col-md-2"><?php include '../view/includes/menu.php'; ?></div>
        <div class="col-md-10 conteudo">

            <div class="row">
                <div class="col-md-12">
                    <div class="page-header"><a class="btn btn-primary novaconta" href="index.php?modulo=portais&tarefa=novaConta"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nova Conta</a>
                        <h1>Contas <small>Cadastro e Atualização de dados</small></h1></div>
                    <?php include '../view/includes/mensagem.php'; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-condensed">
                        <thead>
                        <tr>
                            <th class="col-md-1">#</th>
                            <th class="col-md-3">E-mail</th>
                            <th class="col-md-3">Nome</th>
                            <th class="col-md-3">URL</th>
                            <th class="col-md-1">Administrador</th>
                            <th class="col-md-1">&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(isset($dados['tabela'])) foreach ($dados['tabela'] as $linha) { ?>
                            <tr>
                                <td class="col-md-1"><?php echo $linha['id']; ?></td>
                                <td class="col-md-1"><?php echo $linha['email']; ?></td>
                                <td class="col-md-1"><?php echo $linha['nome']; ?></td>
                                <td class="col-md-1"><?php echo $linha['url']; ?></td>
                                <td class="col-md-1"><?php echo ($linha['admin'] == 1)? "Sim" : "Não"; ?></td>
                                <td class="col-md-1">
                                    <a class="btn btn-primary botao" href="index.php?modulo=portais&tarefa=editarConta&id=<?php echo $linha['id'];?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                                    <a class="btn btn-danger botao" href="index.php?modulo=portais&tarefa=apagarConta&id=<?php echo $linha['id'];?>"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                    <p class="paginacao">
                        <?php if($dados['inicio'] > 0) { ?>
                            <a href="index.php?modulo=portais&tarefa=verContas&inicio=<?php echo $dados["inicio"]-10; ?>"><button class="btn btn-danger">Anterior</button></a>
                        <?php } ?>
                        <a href="index.php?modulo=portais&tarefa=verContas&inicio=<?php echo $dados["inicio"]+10; ?>"><button class="btn btn-primary">Próxima</button></a>
                    </p>
                </div>
            </div>
        </div>

        <?php include '../view/includes/rodape.php'; ?>
    </div>
</body>
</html>