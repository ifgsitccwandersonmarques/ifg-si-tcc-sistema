<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Notitia | Alteração de Conta</title>
    <?php include '../view/includes/head.php'; ?>
    <link rel="stylesheet" type="text/css" href="../view/assets/css/dashboard.css">
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-2 col-md-2 top"><?php include '../view/includes/logo_top.php'; ?></div>
        <div class="hidden-xs col-sm-10 col-md-10 top"><?php include '../view/includes/data_top.php'; ?></div>
    </div>
    <div class="row">
        <div class="col-md-2"><?php include '../view/includes/menu.php'; ?></div>

        <div class="col-md-10 conteudo">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header"><h1>Conta <small>Atualização de dados</small></h1></div>
                    <?php include '../view/includes/mensagem.php'; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form method="POST" class="editarconta" action="index.php?modulo=portais&tarefa=editarConta&id=<?php echo $dados['id'];?>">
                        <div class="form-group">
                            <label for="token">Token de segurança:</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-barcode" aria-hidden="true"></span></span>
                                <input type="text" placeholder="Token" name="token" class="form-control" readonly value="<?php echo $dados['token']; ?>">
                            </div>
                        </div>
                        <label for="email">E-mail:</label>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span></span>
                                <input type="text" placeholder="E-mail" name="email" class="form-control" value="<?php echo $dados['email']; ?>">
                            </div>
                        </div>
                        <label for="senha">Senha  (Deixe em branco para não alterar):</label>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></span>
                                <input type="password" placeholder="Senha (Deixe em branco para não alterar)" name="senha" class="form-control" value="">
                            </div>
                        </div>
                        <label for="email">Nome do Portal:</label>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-font" aria-hidden="true"></span></span>
                                <input type="text" placeholder="Nome do Portal" name="nome" class="form-control" value="<?php echo $dados['nome']; ?>">
                            </div>
                        </div>
                        <label for="email">URL:</label>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></span>
                                <input type="text" placeholder="URL" name="url" class="form-control" value="<?php echo $dados['url']; ?>">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </form>
                </div>
            </div>
        </div>

        <?php include '../view/includes/rodape.php'; ?>
    </div>
</body>
</html>