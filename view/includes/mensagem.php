<?php if(isset($dados['mensagem'])) : ?>
    <div class="alert alert-dismissible alert-<?php echo $dados['tipo']; ?>">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo $dados['mensagem']; ?>
    </div>
<?php endif; ?>