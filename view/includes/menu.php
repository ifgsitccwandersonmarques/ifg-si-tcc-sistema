<div class="lateral">
    <ul class="list-unstyled">
        <li class="titulo">Menu</li>
        <li class="<?php echo ($_GET['tarefa'] == "index" || $_GET['tarefa'] == "login"|| empty($_GET['tarefa']))? "ativo" : ""; ?>"><a href="index.php?modulo=portais&tarefa=index"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Dashboard</a></li>
        <?php if($_SESSION['admin'] == 1) { ?>
        <li class="<?php echo ($_GET['tarefa'] == "verContas" || $_GET['tarefa'] == "editarConta" || $_GET['tarefa'] == "apagarConta" || $_GET['tarefa'] == "novaConta")? "ativo" : ""; ?>"><a href="index.php?modulo=portais&tarefa=verContas&inicio=0"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Contas</a></li>
        <?php } else { ?>
            <li class="<?php echo ($_GET['tarefa'] == "editarConta")? "ativo" : ""; ?>"><a href="index.php?modulo=portais&tarefa=editarConta"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Conta</a></li>
        <?php } ?>
        <li class="<?php echo ($_GET['tarefa'] == "verRelatorios")? "ativo" : ""; ?>"><a href="index.php?modulo=portais&tarefa=verRelatorios&inicio=0"><span class="glyphicon glyphicon-stats" aria-hidden="true"></span> Relatórios</a></li>
        <li><a target="_blank" href="../view/media/docs/manual-integracao.pdf"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Manual de Integração</a></li>
        <li><a href="index.php?modulo=portais&tarefa=logout"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Sair</a></li>
    </ul>
</div>